## iot_poc

#### Only a proof of concept

### Preliminary experiments:


* [Tests of Relay actuator using the Control Panel (Single Page App) and a Web Device page](https://youtu.be/1xJLapAvFys)
    * ![.](https://bitbucket.org/velosomarcus/iot_poc/raw/23addca4a725c4d546b4bbc3e2673bad211696e1/dev-relay-v3.jpg Watch the video)
    * \#ESP8266 \#WiFi \#Websocket \#Microservice \#MongoDB \#MQTT


* [Tests of a Car with a robotic arm that can grab and carry an object between two points following a lined course](https://youtu.be/Ir9XpCjgkgM)
    * ![.](https://bitbucket.org/velosomarcus/iot_poc/raw/23addca4a725c4d546b4bbc3e2673bad211696e1/dev_car_arm_v1.png Watch the video)
    * \#ESP8266 \#Arduino \#WiFi \#I2C


* [Tests of Watering the Plants](https://youtu.be/xcvRWib8xsk)
    * ![.](https://bitbucket.org/velosomarcus/iot_poc/raw/23addca4a725c4d546b4bbc3e2673bad211696e1/dev_watering.jpg Watch the video)
    * \#Arduino


* [Tests of Door Status device and the Web App](https://youtu.be/JMceOPaLL4w) 
    * ![.](https://bitbucket.org/velosomarcus/iot_poc/raw/265dd86d6cbfc514e5b38a56016ae16a9e25b46f/dev_door_v1.png Watch the video)
    * \#ESP8266 \#WiFi \#Websocket


* [Tests of Door Lock device and the Android App](https://youtu.be/0F-G--c5A_c)
    * ![.](https://bitbucket.org/velosomarcus/iot_poc/raw/23addca4a725c4d546b4bbc3e2673bad211696e1/dev_lock_v1.png Watch the video)
    * \#Arduino \#Bluetooth \#NFC \#Android